# Game design document #

Ce document a pour objectif de décrire le jeu Open Merx Online.

# Vue d'ensemble #

Nom : à déterminer
Genre : Jeux de commerce et de stratégie en ligne
Plateforme : client PC pour débuter

## Introduction ##

Open merx online est un projet de jeu de commerce se déroulant dans un univers
parallèle inspiré par le Steam punk. Cet univers est hébergé en ligne et est
persistant. Le joueur représente la leader d'une organisation dans ce monde
austère et cherche à être reconnu.

## Point originaux ##

-   Chaine de production et de commercialisation est gérées par les joueurs

-   Gestion de territoire

-   Collaboration et/ou concurrence entre joueur

-   Univers persistent donc évoluant sans arrêt

## Univers ##

La terre2 est une planète aride en surface, avec une activité volcanique intense et des nappes phréatiques souterraines. Ces éléments ont permis l'apparition de la vie autour des sites géothermiques grace à la présence d'eau et de chaleur. L'évolution permis ensuite l'apparition de la flore, la faune et enfin des humains2. Ceux-ci ont migrés d'oasis en oasis, c’est ainsi que les peuples ont se sont diversifié, langage, agriculture, le travail du fer et ce jusqu'à la maitrise de la vapeur et des machines. Des citées furent créées mais la distance entre chacun d'elle limitait les échanges. Jusqu'en l'an XXXX où la découverte du thale vient bouleverser les choses. Le Thale est un crystal qui chauffé, se transforme en gel et qui le reste même à température ambiante. Ce gel, une fois mis en contact avec un catalyseur, repasse sous forme cristalline en libérant l'énergie qu'il avait accumulé. Un litre de thale peut transformer 1000 litre d'eau en vapeur lors de sa cristallisation. Ce thale est la base de la pile à vapeur, bidon rempli d'eau dans lequel une poche de gel est installée permetant à l'eau de devenir vapeur. Enfin !!! L'humain 2 pouvait transporter la vapeur avec lui et utiliser sa puissance en dehors des cités, c'était le début de la révolution, de la mondialisation.

## Histoire ##

Vous êtes un jeune entrepreneur qui vient d'obtenir son prêt de la banque et qui se lance dans le monde de l'entrepreneuriat. A vous de choisir le chemin pour récolter gloire et richesses. Serez vous un grand commercant, un brigant réputé, le chef d'un conglomerat de production d'armement???


# Définitions #

Voici une liste des définitions utiles pour la compréhension du reste du
document :

- Citée : Une cité est un ensemble de bâtiments situé autour d’une source géothermique. Elle offre un certain nombre de service et est propriété d’une organisation. Beucoup des citées sont sous le contrôle des organisations non joueur.

- Expédition : C'est un groupe qui s'éloigne d'une citée avec un certain objectif. Une fois l'objectif rempli, ils reviennent à la cité... si tout s'est bien passé.

- Organisation : C’est une personne morale qui contrôle un ensemble d’avoir comme des employés, des véhicules ou des citées. (Analogue à une entreprise dans la réalité), un joueur gère une organisation.


# Mécanismes #

Cette section décrit d'une façon générale les fonctoinalités du jeu. Les détails, en terme de valeurs ou de formules, seront précisés dans une autre section.

## La carte ##

La carte du monde est en théorie illimitée. On dit en théorie car les ressources
rendront impossible les expéditions au-delà d’une certaine distance.

La carte est très dense au centre et plus on s’éloigne, plus les citées seront
espacées et plus on aura de probabilité d'y trouver des ressources rares.

Le biotope est principalement composé de désert, montagne aride et toundra. Les
colonies sont situées autour de point géothermiques, source d’eau et d’énergie.

## Exploration ##

Le joueur peut envoyer des groupes effectuer des missions d’exploration. Le
joueur indiquera la zone où envoyer le groupe et les véhicules qui en feront
parti. L’expédition tentera de localiser des sites d’intérêts pendant un temps
et reviendra a sa station de départ pour livrer les résultats. L'exploration est une forme d'expédition.

## Exploitation ##

L’exploitation est l’envoie d’un groupe pour prélever des ressources d’un site
exploré précédemment. Il faudra avoir un groupe équipé en conséquence en
fonction du type de ressources à exploiter. Le groupe se verra affecté une
mission et continuera tant que la mission n’est pas soit remplie soit impossible
à remplir. L'exploitation est également une forme d'expédition.

## Production ##

Dans le jeu, le joueur pourra transformer certaines ressources en autre chose
pouvant soit être vendu soit utilisé a son propre compte. Cela s’effectue dans
des usines, situées dans les citées. Pour réaliser une telle production, le joueur doit posséder un entrepôt où se trouvent les ressources à transformer ainsi qu'une recette. Après avoir payé le tarif de l'usine et attendu le temps nécessaire, il recevra les produits transformés.

### Recettes ###

Les recettes sont des plans qui permettent aux usines de transformer certaines
ressources en autre chose. Une Recette nécessite une certaine quantité de
ressource en entrée et une fois terminé produit des ressources. Cette opération
dure un certain temps, celui-ci est indiqué dans la recette.

## Commerce ##

Afin de réaliser les actions qu'il désire, le joueur aura besoin soit d'argent soit de ressources. Le commerce est utile pour cela. En effet, on peut y acheter des resources dans le but de les transformer, les utiliser ou de les vendre, à profits, sur le marché d'une autre cité. 

## Les véhicules ##

Les expéditions envoyés par le joueur seront constituées de un ou plusieurs véhicules. Il existe différent type de véhicule et ce pour différent type de mission. Les véhicule sont soit produit par le joueur soit achetés sur le marché. Certain véhicule peuvent voler même si la majorité sont terrestre. Les véhicules sont stockés dans les entrepots des organisations quand ils ne sont pas utilisés. La majorité des véhicule sont pilotés par une personne, quelques fois certain poste additionels peuvent être occupé par d'autre personnes. Enfin, certain véhicule seront autonomes mais souvent moins efficaces.

## Ressources humaines ##

Les organisation ont besoin de main d'oeuvre pour pouvoir réaliser les tâches qui leur permettent de prendre de la valeur. Ces employés sont embauché et ont des capacités diverse qui évoluent avec l'experience et la formation. Biensur qui dit employé dit salaire, il faudra donc que le gestionnaire maximise leur utilisation tout en essayant d'éviter au maximum de les perdre dans de tragique accidents....

## Customisation des véhicules ##

Lorsque des expédition,constitués d'une flotte de véhicule, partent en mission, les véhicules pourront être modifiés afin de leur permettre d'effectuer certaines tâches ou simplement d'être plus efficace. On peut imaginer une augmentation du blindage en contrepartie d'une perte en espace de stockage pour un véhicule partant dans une zone a risque. 

## Combats ##

Enfin, un sujet qui touches toutes les expéditions, il existe un risque de rencontrer des groupes dont les intentions ne sont pas amicales. Il faut garder en tête que même si le joueur n'est pas en ligne, l'expédition suit son court et les employés doivent savoir réagir en cas de pépin. Pour ce faire, le joueur pourra donner des consignes au moment de planifier l'expédition. Il pourra également gérer le risque en ajoutant des véhicules d'escorte à cette expédition et préparer les configuration de chaque véhicule en conséquence. C'est le point clé de la gestion du risque. Les combats sont gérés par le serveur, le joueur pour revoir les événements d'un combat pour revoir ses stratégies futures.

## Gestion du territoire ##

Le joueur pourra installer des postes d'exploration, sorte de mini citée. Ceux-ci offriront un certain nombre de services mais devront être protégés. Cette fonctionnalité n'est pas prévue dans la 1ere version, mais c'est ce qui rendra le jeu intéressant à terme.

# Graphismes #

Il existe pour la 1ere version qu'une seule "vue" du jeu. La vue depuis l'entrepot d'une citée. La vue est surtout esthétique, elle pourra afficher certaines informations de la cité. Cette vu propose à l'utilisateur des boutons ouvrant des interfaces permetant d'effectuer les démarches nécessaire de gestion.

## l'entête ##
Un  barre en haut de l'écran permet de voir l'entrepot qui est affiché, l'état du portefeuille de l'organisation et son nom. 


# Données #

## Ressources ##

les infos concernant les resources sont disponible dans le fichier Resources.csv

## Recettes  ##

Description des recettes disponibles dans le jeu. Certaines sont disponibles aux
joueurs, d’autre non.

le document Recipies.txt contient pour le moment les recettes en json.
Les recettes 1-10 sont les recettes de rafinage de roche (transformer des roches en matériaux industriels tel le thale)

## Commerce des cités

Les citées vont compenser les parties des mécaniques non accessibles aux joueur. Au moment présent, les cités vont être en charge de transformer les roches et donc créer un marché pour ces ressources.
