# Merx Online

# Contribution

Comme le suggère le côté open source du projet, les contributions sont acceptées et même très appréciées.
Pour savoir comment contribuer, consultez le fichier CONTRIBUTING.md.

# Mise en place du projet

Cette section vous présente comment récupérer le projet et le lancer. 

Pour pouvoir lancer le projet vous aurez besoin :

	visual studio 2017
	Unity (normalement la derniere version)
	l'api c# de smartfoxServer : https://www.smartfoxserver.com/download#p=client

La marche à suivre : 
- Cloner le projet 
- Récupérer l'api SmartfFoxServer : https://www.smartfoxserver.com/download#p=client
- Extraire le dossier Release de l'api dans le dossier open-merx-online\ClientUnity\MerxOnlineClient\Assets\Plugins\Smartfox (pour avoir smartfox.dll et le dossier Unity_WebGl dans ce dossier)
- Supprimer le dossier 	UWP dans open-merx-online\ClientUnity\MerxOnlineClient\Assets\Plugins\Smartfox
- ouvrir la solution \Server\solutions\vs2015\MerxOnlineServer\MerxOnlineServer.sln
- Pour le projet ServerApp, ajouter la reference vers smartfox.dll si elle n'est pas trouvée
- Choisir une configuration DebugUnity ou ReleaseUnity
- La compiler (normalement les binaires nécessaires seront copiés dans le dossier \ClientUnity\MerxOnlineClient\Assets\Plugins
- Lancer unity en ouvrant le projet \ClientUnity\MerxOnlineClient
- Aller dans le dossier assets/plugins/Smartfox 
- Selectionner smartfox.dll, dans les options cocher "any platform" et "exlude platform WebGL" et apply
- Aller dans le dossier assets/pugins/Smartfox/Unity_WebGL
- Selectionner SmartFox2X_WebGL.dll, dans les options décocher "any platform" et cocher "Include platform WebGL" et apply
- ouvrir la scene \ClientUnity\MerxOnlineClient\Assets\ScenesPlayScene.unity
- faire play....