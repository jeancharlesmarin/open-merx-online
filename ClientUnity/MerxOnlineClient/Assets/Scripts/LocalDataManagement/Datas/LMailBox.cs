﻿using System.Collections.Generic;

/// <summary>
/// C'est une classe qui permet de contenir l'information de la boite de messagerie du joueur. 
/// On y retrouver les messages recus et elle permet d'envoyer des messages au autres
/// </summary>
public class LMailBox {

    public delegate void OnChangeAction();
    public OnChangeAction OnChange = delegate { };

    MerxStructures.MailBoxInfos _infos = null;

    public void UpdateInfos(MerxStructures.MailBoxInfos infos) {
        _infos = infos;
        OnChange();
    }

    public List<MerxStructures.MailInfos> GetMails() {
        if (null == _infos)
            return null;

        return new List<MerxStructures.MailInfos>(_infos.mails);
    }
}
