﻿using System.Collections.Generic;

using MerxMessage;

/// <summary>
/// Structure qui represente la liste des hangars dont un joueur peut acceder
/// </summary>
public class LPlayerHangarsList {

    public delegate void OnChangeAction();
    public event OnChangeAction OnLoadingDone = delegate {};
    public event OnChangeAction OnChange = delegate { };

    public bool IsLoading { get; private set; }

    LocalDataManager _localDataManager = null;
    int _localPlayerID = 0;

    List<LHangar> _hangars = null;

    public LPlayerHangarsList(int localPlayerID, LocalDataManager dataManager) {
        _localPlayerID = localPlayerID;

        IsLoading = false;
        _localDataManager = dataManager;

        _localDataManager.OnNewMessage += OnNewMessage;
        LocalDataManager.Instance.Universe.OnNewHangar += OnNewHangar;


        _hangars = new List<LHangar>();
    }

    ~LPlayerHangarsList() {
        _localDataManager.OnNewMessage -= OnNewMessage;
    }

    void OnNewMessage(Message m) {

        if (!m.IsAnswer)
            return;

        RequestHangarList r = m as RequestHangarList;
        if (null != r) {
            if (r.FromPlayerID == LocalDataManager.Instance.LocalPlayerID) {
                foreach(var s in r.hangars) {
                    LCity c = LocalDataManager.Instance.Universe.GetCity(s.cityID);
                    LHangar h = c.AddHangar(s.corpID);
                    h.Update(s);
                }
                IsLoading = false;
                OnLoadingDone();
            }
        }
    }

    private void OnNewHangar(LHangar hangar) {
        if(hangar.CorpID == LocalDataManager.Instance.LocalPlayerID) {
            _hangars.Add(hangar);
            OnChange();
        }
    }

    public List<LHangar> GetHangars() {
        if (null == _hangars)
            return new List<LHangar>();

        return new List<LHangar>(_hangars);
    }
}
