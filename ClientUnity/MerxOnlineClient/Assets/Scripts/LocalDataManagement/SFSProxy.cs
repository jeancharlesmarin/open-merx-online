﻿using System.Collections.Generic;

using UnityEngine;

using MerxMessage;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Logging;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using System.Security.Cryptography;
using System.Text;

public class SFSProxy : MonoBehaviour, IEventProxy, IRequestProxy {

    private Queue<Message> _message = new Queue<Message>();
    /// <summary>
    /// l'id local du player une fois connecté
    /// </summary>
    public int LocalID { get; private set; }

    SmartFox _smartFox = null;
    bool _ready = false;

    string _username = "";
    string _password = "";

    public void Connect() {
        //lancer la connection
#if UNITY_WEBGL
        _smartFox = new SmartFox(Sfs2X.Util.UseWebSocket.WS_BIN, false);
#else
        _smartFox = new SmartFox();
#endif

        Sfs2X.Util.ConfigData cfg = new Sfs2X.Util.ConfigData();
        cfg.Host = "51.254.200.78";
        //cfg.Host = "127.0.0.1";
#if UNITY_WEBGL
        cfg.Port = 8080;
#else
        cfg.Port = 9933;
#endif
        cfg.Zone = "OpenMerx";
        cfg.Debug = false;

        _smartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        _smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        _smartFox.AddEventListener(SFSEvent.LOGIN, OnLogin);
        _smartFox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        _smartFox.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
        _smartFox.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
        _smartFox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtension);

        _smartFox.AddLogListener(LogLevel.INFO, OnInfoMessage);
        _smartFox.AddLogListener(LogLevel.WARN, OnWarnMessage);
        _smartFox.AddLogListener(LogLevel.ERROR, OnErrorMessage);

        Debug.Log("connecting to Smartfox");

        _smartFox.Connect(cfg);


    }

    private void OnDestroy() {
        _smartFox.Disconnect();
        _smartFox = null;
    }

    public void Login(string username, string password) {
        _username = username;
        _password = password;
        _smartFox.Send(new LoginRequest(_username, MerxUtils.MerxCrypto.ComputeSha256Hash(_password)));
    }

    public bool Ready() {
        return _ready;
    }

    public bool Connected() {
        return null != _smartFox && _smartFox.IsConnected;
    }

    private void Reset() {
    }

    public void Update() {
        if (_smartFox != null) {
            _smartFox.ProcessEvents();
        }
    }

    void OnConnection(BaseEvent evt) {
        Debug.Log("OnConnection");
        if ((bool)evt.Params["success"]) {
            Debug.Log("Connection established successfully");
            Debug.Log("SFS2X API version: " + _smartFox.Version);
            Debug.Log("Connection mode is: " + _smartFox.ConnectionMode);            
        } else {
            Debug.Log("Connection failed; is the server running at all?");

            // Remove SFS2X listeners and re-enable interface
            Reset();
        }
    }

    void OnConnectionLost(BaseEvent evt) {
        Debug.Log("OnConnectionLost");
    }

    void OnLogin(BaseEvent evt) {
        Debug.Log("OnLogin");
        User user = (User)evt.Params["user"];
        ISFSObject datas = (ISFSObject) evt.Params["data"];
        LocalID = datas.GetInt("ID");

        // Show system message
        string msg = "Connection established successfully\n";
        msg += "SFS2X API version: " + _smartFox.Version + "\n";
        msg += "Connection mode is: " + _smartFox.ConnectionMode + "\n";
        msg += "Logged in as " + user.Name;
        Debug.Log(msg);


        _smartFox.Send(new JoinRoomRequest("The Lobby"));
    }

    void OnLoginError(BaseEvent evt) {
        Debug.Log("OnLoginError");
    }

    void OnRoomJoin(BaseEvent evt) {
        _ready = true;

        Debug.Log("OnRoomJoin");
    }

    void OnRoomJoinError(BaseEvent evt) {
        Debug.Log("OnRoomJoinError");
    }

    void OnInfoMessage(BaseEvent evt) {
        string message = (string)evt.Params["message"];
        Debug.Log("INFO : " + message);
    }

    void OnWarnMessage(BaseEvent evt) {
        string message = (string)evt.Params["message"];
        Debug.Log("WARN : " + message);
    }

    void OnErrorMessage(BaseEvent evt) {
        string message = (string)evt.Params["message"];
        Debug.Log("ERROR : " + message);
    }

    public Queue<Message> GetMessages() {
        Queue<Message> result = _message;
        _message = new Queue<Message>();
        return result;
    }

    public void SendRequest(Message message) {
        if (null == _smartFox || !_ready)
            return;

        message.FromPlayerID = LocalID;

        SFSObject obj = new SFSObject();

        Sfs2X.Util.ByteArray array = new Sfs2X.Util.ByteArray(message.Serialize());

        obj.PutByteArray("message", array);
        obj.PutUtfString("messageName", message.GetType().ToString());

        //_smartfox.Send(new ExtensionRequest("sendToServer", obj, null));           
        _smartFox.Send(new ExtensionRequest("sendToServer", obj, _smartFox.LastJoinedRoom));

    }

    void OnExtension(BaseEvent evt) {
        System.Console.WriteLine("OnExtension");

        string cmd = (string)evt.Params["cmd"];
        SFSObject dataObject = (SFSObject)evt.Params["params"];

        if (cmd == "sendToClients") {
            Sfs2X.Util.ByteArray bytes = dataObject.GetByteArray("message");

            Message m = Message.Deserialize(bytes.Bytes);

            if (null != m) {
                _message.Enqueue(m);
                System.Console.WriteLine("reveived : " + m.ToString());
            }

        }
    }
}

