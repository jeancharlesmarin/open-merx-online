﻿
using System.Collections.Generic;
using System.IO;

#if !UNITY_WEBGL
using System.Threading;
#endif

using MerxMessage;

public class LocalEventProxy : IEventProxy{

    private Queue<MemoryStream> _inStream = null;
#if !UNITY_WEBGL
    private Thread _readThread = null;
#endif


    private Queue<Message> _message = new Queue<Message>();

    public LocalEventProxy(Queue<MemoryStream> stream) {
        _inStream = stream;
#if !UNITY_WEBGL
        _readThread = new Thread(new ThreadStart(ThreadLoop));
        _readThread.Start();
#endif
    }

    public Queue<Message> GetMessages() {
        Queue<Message> result = _message;
        _message = new Queue<Message>();
        return result;
    }
#if !UNITY_WEBGL
    private void ThreadLoop() {
        while (true) {
            Thread.Sleep(0);
            lock (_inStream) {
                if(_inStream.Count > 0) {

                    MemoryStream ms = _inStream.Dequeue();
                    Message m = Message.Deserialize(ms.ToArray());
                    lock (_message) {
                        _message.Enqueue(m);                        
                    }
                }                
            }
        }
    }
#endif
}
