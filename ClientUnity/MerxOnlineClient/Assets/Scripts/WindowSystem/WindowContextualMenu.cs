﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Component d'un menu contextuel en cours d'afficage
/// </summary>
public class WindowContextualMenu : MonoBehaviour {

    [SerializeField]
    GameObject linePrefab = null;

    public delegate void OnLineClicAction(string line);
    public OnLineClicAction onLineClickCB = null;

    public void AddChoice(string line, OnLineClicAction cb) {
        GameObject o = Instantiate<GameObject>(linePrefab);
        o.transform.SetParent(this.transform);
        TMPro.TextMeshProUGUI text = o.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        text.text = line;

        Button b = o.GetComponentInChildren<Button>();
        b.onClick.AddListener(() => { cb(text.text); });
    }

}
