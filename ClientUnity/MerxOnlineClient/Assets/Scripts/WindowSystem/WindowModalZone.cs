﻿
using System;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// le component a ajouter sur la zone modale, il permet entre autre de gerer les 
/// clics hors zone lors d'afficage modaa
/// </summary>
public class WindowModalZone : MonoBehaviour , IPointerClickHandler{

    public delegate void OnClicAction();
    public event OnClicAction OnClic = delegate { };

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        OnClic();
    }
}
