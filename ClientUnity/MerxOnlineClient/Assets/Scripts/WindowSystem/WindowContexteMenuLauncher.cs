﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Il s'agit du component a mettre sur un objet qui peut afficher un menu contextuel
/// </summary>
public class WindowContexteMenuLauncher : MonoBehaviour, IPointerDownHandler, IPointerClickHandler {
    public delegate List<string> GetMenuLinesAction();
    public GetMenuLinesAction GetMenuLinesCB { get; set; }

    public delegate void OnMenuItemClickAction(string lineName);
    public OnMenuItemClickAction OnMenuLineClic = delegate { };

    WindowSystem _ws = null;
    WindowContextualMenu _menu = null;

    private void Awake() {
        _ws = WindowSystem.Instance;
    }

    /// <summary>
    /// ici on lance le menu si on fait un clic gauche sur l'item et que la liste
    /// des choix possibles est non vide
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Right) {
            if (null != GetMenuLinesCB) {
                //lancer la coroutine de surveillance de clic
                StartCoroutine(CheckForClick());

                List<string> lines = GetMenuLinesCB();
                if (lines.Count > 0) {
                    //instancier la boite du menu
                    _menu = _ws.ShowMenu();

                    //ajouter les lignes du menu
                    foreach (string s in lines) {
                        _menu.AddChoice(s, OnLineClick);
                    }
                }
            }
        }
    }

    /// <summary>
    /// coroutine qui permet de tester si un clic de souris est fait en dehors du menu, si c'est le cas
    /// on detruit le menu, si c'est dans le menu alors la ligne du menu gerera elle meme son clic
    /// </summary>
    /// <returns></returns>
    private IEnumerator CheckForClick() {
        while (true) {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)) {
                if (EventSystem.current.IsPointerOverGameObject()) {

                    if(null == EventSystem.current.currentSelectedGameObject ||
                        EventSystem.current.currentSelectedGameObject.GetComponentInParent<WindowContextualMenu>() == null) {
                        destroyMenu();
                        yield break;
                    }
                }
            }
            yield return null;
        }
    }

    /// <summary>
    /// event levé quand on fait un bouton de souris es appuyé avec le curseur sur l'item
    /// il est implémenté car sans lui le OnPointerClick ne fonctoinne pas.... étrange
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData) {
        eventData.Reset();
    }

    /// <summary>
    /// methode appelé qunad une ligne est choisie du menu
    /// </summary>
    /// <param name="lineName"></param>
    private void OnLineClick(string lineName) {
        //detruire le menu
        destroyMenu();

        StopAllCoroutines();

        //informer les abonne que le menu a une selection
        OnMenuLineClic(lineName);
    }


    private void destroyMenu() {
        //detruire le gameObject menu
        if (null != _menu) {
            Destroy(_menu.gameObject);
            _menu = null;
        }
    }
}
