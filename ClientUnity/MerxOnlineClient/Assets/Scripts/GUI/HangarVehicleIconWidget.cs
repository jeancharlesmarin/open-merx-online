﻿
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class HangarVehicleIconWidget : MonoBehaviour {

    public LHangar Hangar { get; private set; }

    public int VehicleID { get; private set; }

    private void Awake() {
        WindowContexteMenuLauncher context = GetComponent<WindowContexteMenuLauncher>();
        if(null != context) {
            context.GetMenuLinesCB = GetContextualLines;
            context.OnMenuLineClic += OnContextualClic;
        }
    }


    public void SetData(LHangar hangar, LVehicle vehicle) {
        VehicleID = vehicle.ID;
        Hangar = hangar;

        Image i = GetComponent<Image>();
        i.sprite = ResourcesData.Instance.GetVehicleInfo(vehicle.VehicleType).icon;
    }

    private List<string> GetContextualLines() {
        List<string> result = new List<string>();
        
        result.Add("demonter");
        result.Add("detruire");

        return result;
    }

    private void OnContextualClic(string line) {
        switch (line) {
            case "demonter":

            MerxMessage.RequestRepackageVehicle r   = new MerxMessage.RequestRepackageVehicle();
            r.cityID = Hangar.City.ID;
            r.vehicleID = VehicleID;

            LocalDataManager.Instance.SendRequest(r);

            break;
        }
    }
}
