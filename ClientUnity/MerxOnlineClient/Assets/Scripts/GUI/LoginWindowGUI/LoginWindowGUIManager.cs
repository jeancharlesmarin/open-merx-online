﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginWindowGUIManager : MonoBehaviour {
    [SerializeField]
    bool local = true;

    [SerializeField]
    TMPro.TMP_InputField loginName = null;

    [SerializeField]
    TMPro.TMP_InputField password = null;

    [SerializeField]
    Transform connectedZone = null;

    [SerializeField]
    OrganisationCreateWidget organisationCreateWidget = null;

    LocalDataManager _manager = null;
    SFSProxy _proxy = null;

    Coroutine _currentCoroutine = null;

    public void Start() {
        organisationCreateWidget.gameObject.SetActive(false);

        //on se connecte avant tout
        if (local) {
            connectedZone.gameObject.SetActive(true);
        } else {
            if (null != _currentCoroutine)
                StopCoroutine(_currentCoroutine);
            _currentCoroutine = StartCoroutine(StartSFSConnection());
            connectedZone.gameObject.SetActive(false);
        }
    } 

    public void Login() {

        //cas d'un simulateur local
        if (local) {
            if (null != _currentCoroutine)
                StopCoroutine(_currentCoroutine);
            _currentCoroutine = StartCoroutine(StartLocalLogin());
        } else {
            //cas d'un simulateur distant
            if (null != _currentCoroutine)
                StopCoroutine(_currentCoroutine);
            _currentCoroutine = StartCoroutine(StartSFSLogin());
        }
    }

    private IEnumerator StartSFSConnection() {
        //créer le localDataManager
        GameObject localDataObj = new GameObject("LocalDataManager");
        _manager = localDataObj.AddComponent<LocalDataManager>();

        _proxy = localDataObj.AddComponent<SFSProxy>();
        DontDestroyOnLoad(localDataObj);

        Debug.Log("Starting connection to SFS");

        _proxy.Connect();
        while (!_proxy.Connected()) {
            yield return null;
        }

        _manager.SetProxy(_proxy, _proxy);

        connectedZone.gameObject.SetActive(true);

        yield break;
    }

    private IEnumerator StartLocalLogin() {

        GameObject localDataObj = new GameObject("LocalDataManager");
        DontDestroyOnLoad(localDataObj);

        _manager = localDataObj.AddComponent<LocalDataManager>();
        _manager.LocalPlayerID = 1;

        LocalSimulator localSim = localDataObj.AddComponent<LocalSimulator>();
        localSim.Init();

        _manager.SetProxy(localSim.EventProxy, localSim.RequestProxy);

        while (!_manager.Ready()) {
            yield return null;
        }

        LoadPlayScene();

        yield break;
    }

    private void GetPlayerDatas() {
        if (null != _currentCoroutine)
            StopCoroutine(_currentCoroutine);
        _currentCoroutine = StartCoroutine(GetPlayersDataProcess());
    }

    private IEnumerator StartSFSLogin() {
               

        _proxy.Login(loginName.text, password.text);

        while (!_proxy.Ready()) {
            yield return null;
        }
        
        _manager.LocalPlayerID = _proxy.LocalID;
        _manager.GetDatas();
        
        while (!_manager.DataOK) {
            yield return null;
        }

        // les données sont recu, on essais de recupere les infos de l'organisation
        GetPlayerDatas();
    }

    private IEnumerator GetPlayersDataProcess() {
        _manager.GetOrganisationDatas();

        while (!_manager.OrganisationOK) {
            yield return null;
        }

        if (_manager.LocalCorporation == null) {
            // il faut creer l'organisation du joueur
            organisationCreateWidget.Show(GetPlayerDatas);
        } else {
            // on a tout ce qu'il faut a priori, on lance le jeu    
            LoadPlayScene();
        }

        yield break;

    }

    private void LoadPlayScene() {                
        UnityEngine.SceneManagement.SceneManager.LoadScene("PlayScene");
    }

}
