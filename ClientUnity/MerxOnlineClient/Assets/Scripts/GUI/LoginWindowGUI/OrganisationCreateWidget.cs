﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Widget permetant la creation d'une organisation quand le joueur n'en a pas
/// </summary>
public class OrganisationCreateWidget : MonoBehaviour {

    [SerializeField]
    Button CreateButton = null;

    private System.Action _callback = null;

    private void Awake() {
        if(null != CreateButton) {
            CreateButton.onClick.AddListener(OnCreateButtonClic);
        }
    }

    public delegate void testAction();

    public void Show(System.Action callback) {
        this.gameObject.SetActive(true);
        _callback = callback;
    }

    /// <summary>
    /// le joueur a cliqué sur le bouton creer une corpo, on envoie le message au serveur
    /// </summary>
    private void OnCreateButtonClic() {
        MerxMessage.RequestOrganisationCreate m = new MerxMessage.RequestOrganisationCreate();

        LocalDataManager.Instance.OnNewMessage += ManagerServerAnswer;
        LocalDataManager.Instance.SendRequest(m);
    }

    void ManagerServerAnswer(MerxMessage.Message m) {
        if(m is MerxMessage.RequestOrganisationCreate) {
            LocalDataManager.Instance.OnNewMessage -= ManagerServerAnswer;
            this.gameObject.SetActive(false);
            // la reponse est arrivé, on relance l'init
            _callback?.Invoke();
        }
    }

}
