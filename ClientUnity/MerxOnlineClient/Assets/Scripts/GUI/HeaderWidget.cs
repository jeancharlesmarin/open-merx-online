﻿using UnityEngine;

public class HeaderWidget : MonoBehaviour {

    [SerializeField]
    TMPro.TextMeshProUGUI _date = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _ICU = null;

    LCorporation _localCorp = null;

    public void SetDate(long date) {
        if (null == _date)
            return;

        _date.text = date.ToString();
    }

    public void SetLocalCorporation(LCorporation corp) {
        _localCorp = corp;
        if (null != _localCorp) {
            _localCorp.OnChange += Corp_OnChange;
        }
        Corp_OnChange();
    }

    private void Corp_OnChange() {
        if (null == _localCorp)
            return;

        _ICU.text = _localCorp.ICU.ToString() + " ICU";
    }
}
