﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using MerxStructures;

public class MarketDataWidget : MonoBehaviour {
    public delegate void OnReturnAction();
    public event OnReturnAction OnReturnClic = delegate { };

    [SerializeField]
    Button _returnButton = null;

    [SerializeField]
    GameObject _loadingView = null;

    [SerializeField]
    Transform _SellOrders = null;

    [SerializeField]
    Transform _BuyOrders = null;

    [SerializeField]
    MarketSellerLineWidget _SellLinePrefab = null;

    [SerializeField]
    MarketBuyerLineWidget _BuyLinePrefab = null;

    LocalDataManager _proxy = null;

    int _currentProdID = 0;

    private void Awake() {
        _proxy = LocalDataManager.Instance;
        _proxy.OnNewMessage += OnMessage;
        SetLoading();
        _returnButton.onClick.AddListener(OnReturnButtonClic);        
    }

    private void OnDestroy() {
        _proxy.OnNewMessage -= OnMessage;
    }

    /// <summary> permet de lancer l'affichage des transactions possible pour un type de ressource donnée </summary>
    /// <param name="name"></param>
    public void SetProduct(string name, int prodId) {
        gameObject.SetActive(true);

        _currentProdID = prodId;

        MerxMessage.RequestMarketDatas res = new MerxMessage.RequestMarketDatas();
        res.resourceID = prodId;

        _proxy.SendRequest(res);        
    }

    private void OnReturnButtonClic() {
        OnReturnClic();
    }

    private void SetLoading() {
        _loadingView.SetActive(true);
    }

    private void OnMessage(MerxMessage.Message m) {
        if(m.IsAnswer && m is MerxMessage.RequestMarketDatas) {
            MerxMessage.RequestMarketDatas r = m as MerxMessage.RequestMarketDatas;

            _loadingView.SetActive(false);

            while(_SellOrders.childCount > 0) {
                Transform t = _SellOrders.GetChild(0);
                t.SetParent(null);
                Destroy(t.gameObject);
            }
            while(_BuyOrders.childCount > 0) {
                Transform t = _BuyOrders.GetChild(0);
                t.SetParent(null);
                Destroy(t.gameObject);
            }

            int playerCorpID = LocalDataManager.Instance.LocalPlayerID;

            foreach (MarketOrder s in r.Orders) {
                if (s.buying) {
                    MarketBuyerLineWidget o = Instantiate<MarketBuyerLineWidget>(_BuyLinePrefab);
                    o.transform.SetParent(_BuyOrders);
                    LCity station = LocalDataManager.Instance.Universe.GetCity(s.cityID);
                    o.SetOrder(s);

                } else {
                    MarketSellerLineWidget o = Instantiate<MarketSellerLineWidget>(_SellLinePrefab);
                    o.transform.SetParent(_SellOrders);
                    LCity station = LocalDataManager.Instance.Universe.GetCity(s.cityID);
                    o.SetOrder(s);
                }
            }

        } else if (m.IsAnswer && m is MerxMessage.RequestFillOrders) {
            MerxMessage.RequestFillOrders o = m as MerxMessage.RequestFillOrders;
            if(o.order.resID == _currentProdID) {
                SetProduct("", _currentProdID);
            }
        }
    }
}
