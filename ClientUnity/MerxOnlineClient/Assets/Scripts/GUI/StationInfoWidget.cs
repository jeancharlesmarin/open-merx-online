﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// C'est une vue qui permet d'afficher les informations d'une station.
/// </summary>
public class StationInfoWidget : WindowContentPrefab {

    private LCity _city= null;

    /// <summary> le champs ou afficher l'image de la stataion  </summary>
    [SerializeField]
    Image _stationImage = null;

    /// <summary> La zone de texte affichant la description de la station  </summary>
    [SerializeField]
    TMPro.TextMeshProUGUI _description = null;

    public void SetCity(LCity city) {
        _city = city;
        _window.Title = _city.Name;

        StationLocalInfo infos = ResourcesData.Instance.GetStationInfos(_city.ID);
        if(null != infos) {
            _stationImage.sprite = infos.icon;
            _description.text = infos.description;
        }
    }
}
