﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// component gerant une vue permetant de preciser les quantites et les autres details
/// d'une transaction sur le market.
/// </summary>
public class MarketOrderDetailWidget : MonoBehaviour {

    [SerializeField]
    Button _okButton = null;

    [SerializeField]
    Button _cancelButton = null;

    [SerializeField]
    TMPro.TMP_InputField _qte = null;

    [SerializeField]
    TMPro.TMP_InputField _price = null;

    [SerializeField]
    TMPro.TMP_InputField _total = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _message = null;

    MerxStructures.MarketOrder _order = null;
    int _minQte = 0;
    int _maxQte = 0;

    int _currentQte = 0;
    int _currentPrice = 0;

    private void Awake() {
        _okButton.onClick.AddListener(OnOkClic);
        _cancelButton.onClick.AddListener(OnCancelClic);

        _qte.onValidateInput = OnQteChange;
        _price.onValidateInput = OnPriceChange;
    }

    public void Show(MerxStructures.MarketOrder order) {
        gameObject.SetActive(true);
        _order = order;

        UpdateBounds();
        _currentQte = _maxQte;
        _currentPrice = _order.price;

        UpdateView();
    }


    public void Hide() {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// ici on determiner les maximum qu'on peut vendre/acheter en fonction des infos qu'on a en local
    /// </summary>
    private void UpdateBounds() {
        _minQte = 1;
        _maxQte = _order.qte;

        if (_order.buying) {
            // le max est le min entre la quantite de l'order et la quantité dans le hangar
            _maxQte = 0;
            LCitiesList list = LocalDataManager.Instance.CitiesList;
            foreach (LCity s in list.GetCities()) {
                if (s.ID == _order.cityID) {
                    LHangar hangar = s.GetHangar(LocalDataManager.Instance.LocalPlayerID);
                    if (null != hangar) {
                        Dictionary<int, int> qtes = hangar.GetQtes();
                        if (qtes.ContainsKey(_order.resID)) {
                            int qte = qtes[_order.resID];
                            if (qte < _order.qte)
                                _maxQte = qte;
                            else
                                _maxQte = _order.qte;
                            break;
                        }
                    }
                }
            }
        } else {
            // le max ne peux pas etre au dela de ce qu'on peut payer
            long icu = LocalDataManager.Instance.LocalCorporation.ICU;
            if (_maxQte * _order.price > icu) {
                _maxQte = System.Convert.ToInt32(icu / _order.price);
            }
        }
    }

    private void OnOkClic() {
        MerxMessage.RequestFillOrders req = new MerxMessage.RequestFillOrders();
        req.order = _order;
        req.qte = int.Parse(_qte.text);

        LocalDataManager.Instance.SendRequest(req);

        Hide();
    }

    private void OnCancelClic() {
        Hide();
    }

    private void UpdateView() {
        _qte.text = _currentQte.ToString();
        _price.text = _currentPrice.ToString();
        _total.text = (_currentQte * _currentPrice).ToString();
        _message.text = "Entre la quantite a acheter entre " + _minQte.ToString() + " et " + _maxQte.ToString();
    }

    private char OnQteChange(string text, int charIndex, char addedChar) {

        int finSelection = _qte.selectionFocusPosition;
        int debutSelection = _qte.selectionAnchorPosition;
        if (debutSelection > finSelection) {
            int swap = debutSelection;
            debutSelection = finSelection;
            finSelection = swap;
        }

        string tmp = text;
        if (finSelection != debutSelection) {
            tmp = tmp.Remove(debutSelection, finSelection - debutSelection);
            tmp = tmp.Insert(debutSelection, addedChar.ToString());
        } else {
            tmp = text.Insert(charIndex, addedChar.ToString());
        }

        int result = 0;
        if (int.TryParse(tmp, out result)) {
            if (result >= _minQte && result <= _maxQte) {
                _currentQte = result;
                return addedChar;
            }
        }

        return '\0';
    }

    private char OnPriceChange(string text, int charIndex, char addedChar) {
        int result = 0;
        string tmp = text.Insert(charIndex, addedChar.ToString());
        if (int.TryParse(tmp, out result)) {
            return addedChar;
        }

        return '\0';
    }
}
