﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationListWidget : WindowContentPrefab {
    [SerializeField]
    StationLineInfoWidget _lineInfoPrefab = null;

    [SerializeField]
    Transform _content = null;

    LCitiesList _citiesList = null;

    private void Awake() {
        //recuperer la liste de stations

        _citiesList = LocalDataManager.Instance.CitiesList;
        _citiesList.OnListChange += _citiesList_OnListChange;
        _citiesList_OnListChange();    
    }

    private void _citiesList_OnListChange() {
        while(_content.childCount > 0) {
            Transform t = _content.GetChild(0);
            t.SetParent(null);
            Destroy(t.gameObject);
        }

        foreach(LCity s in _citiesList.GetCities()) {
            StationLineInfoWidget line = Instantiate<StationLineInfoWidget>(_lineInfoPrefab);            
            line.transform.SetParent(_content);
            line.SetCity(s);
        }
    }
}
