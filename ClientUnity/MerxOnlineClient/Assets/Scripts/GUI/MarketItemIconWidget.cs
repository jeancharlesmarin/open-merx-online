﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MarketItemIconWidget : MonoBehaviour {

    public delegate void OnClicAction(string name, int prodID);
    public event OnClicAction OnClic = delegate {};

    [SerializeField]
    TMPro.TextMeshProUGUI productName = null;

    [SerializeField]
    Image itemImage = null;

    int _prodID = 0;
    string _productName = "";

    private void Awake() {
        Button b = GetComponent<Button>();
        b.onClick.AddListener(() => { OnClic(_productName, _prodID); } );
    }

    public void SetItem(string name, int prodId) {
        _productName = name;
        if(null != productName)
            productName.text = name;

        _prodID = prodId;

        itemImage.sprite = ResourcesData.Instance.GetSprite(prodId);
    }
}
