﻿using UnityEngine;

public class ShortcutWidget : MonoBehaviour {

    WindowSystem _ws = null;

    private void Awake() {
        _ws = GetComponentInParent<WindowSystem>();
    }

    public void RequestHangars() {

        Window w =_ws.NewWindow("HangarList");
        w.Show();
    }

    public void RequestCities() {

        Window w = _ws.NewWindow("CitiesList");
        w.Show();
    }

    public void OpenMarket() {
        Window w = _ws.NewWindow("Market");
        w.Show();
    }
    public void OpenExpeditionPreparation() {
        Window w = _ws.NewWindow("ExpeditionPreparation");
        w.Show();
    }

    public void OpenMailBox() {
        Window w = _ws.NewWindow("MailBox");
        w.Show();
    }
    
}
