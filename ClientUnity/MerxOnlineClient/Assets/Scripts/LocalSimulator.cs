﻿using System.Collections.Generic;

using UnityEngine;

public class LocalSimulator : MonoBehaviour {

    [SerializeField]
    int _playerID = 0;

    [SerializeField]
    TextAsset _savegamefile = null;

    MerxServer.MerxServerApp _serverApp = null;

    IRequestProxy _requestProxy = null;
    LocalEventProxy _eventProxy = null;

	// Use this for initialization
	void Awake () {
        Init();
	}

    public int Init() {
        if (_requestProxy != null)
            return -1;

        Queue<System.IO.MemoryStream> client2ServerStream  = new Queue<System.IO.MemoryStream>();
        Queue<System.IO.MemoryStream> server2ClientStream = new Queue<System.IO.MemoryStream>();


        _requestProxy = new LocalRequestProxy(client2ServerStream);
        _eventProxy = new LocalEventProxy(server2ClientStream);

        MerxServer.LocalRequestReceiver reqReceiver = new MerxServer.LocalRequestReceiver(client2ServerStream);
        MerxServer.LocalEventSender eventSender = new MerxServer.LocalEventSender(server2ClientStream);
        reqReceiver.LocalPlayerID = _playerID;

        if (null != _savegamefile && _savegamefile.bytes.Length > 0) {
            _serverApp = new MerxServer.MerxServerApp(_savegamefile.bytes);
        } else {
            _serverApp = new MerxServer.MerxServerApp();
        }

        _serverApp.RequestReceiver = reqReceiver;
        _serverApp.EventSender = eventSender;
        _serverApp.Start();

        return _playerID;
    }

    private void OnDestroy() {
        _serverApp.Stop();
    }

#if UNITY_EDITOR
    //sauver dans le fichier de sauvegarde
    [ContextMenu("Save Game")]
    void DoSomething() {
        if (null == _savegamefile)
            return;


        byte[] saveData = _serverApp.GetSaveData();

        string path = UnityEditor.AssetDatabase.GetAssetPath(_savegamefile);
        System.IO.File.WriteAllBytes(path, saveData);

        
        Debug.Log("saved");
    }

#endif

    public IRequestProxy RequestProxy { get { return _requestProxy; } }

    public LocalEventProxy EventProxy { get { return _eventProxy; } }
}
