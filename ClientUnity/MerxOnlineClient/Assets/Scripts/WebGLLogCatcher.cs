﻿using UnityEngine;

public class WebGLLogCatcher : MonoBehaviour
{

    [SerializeField]
    TMPro.TextMeshProUGUI console = null;



    // Start is called before the first frame update
    void Start()
    {
#if UNITY_WEBGL
        Application.logMessageReceived += Application_logMessageReceived;  
#endif
    }

    private void Application_logMessageReceived(string condition, string stackTrace, LogType type) {
        console.text = condition + "\n" + console.text;
    }

}

