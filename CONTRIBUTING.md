﻿# Contribution

Open merx online étant un projet open source, un des objectifs est de permettre aux gens intéressés de participer à son développement. C'est ce qu'on appelle les contributions. Les contributions peuvent prendre différentes formes, code, graphisme, game design, traduction, orthographe ou simplement donner des avis.

# Comment contribuer

## Preparer l'environement de dévelopement

Ce qu'il faut : 
 - Pour le client unity
   - Visual studio 2017
   - unity3D
 - pour le serveur
   - eclipse pour java
   - SmartfoxServer 2X

Procédure de préparation : 

 - Installer le nécessaire pour le client Unity
   - Voir le readme.md   
 - Pour préparer l'environement du serveur de communication (Smartfox)
   - Installer smartfoxserver https://www.smartfoxserver.com/download#p=installer
   - Ajouter une variable d'environement SFS_home pointant vers la racine de Smartfox(ex : SFS_home=D:\Sandbox\Tools\SmartFoxServer_2X\SFS2X)
   - Installer un IDE pour java (eclipse par exemple)
   - Copier le fichier OpenMerx.zone.xml de "open-merx-online\Server\Configs" vers "<installSmartfox>\SFS2X\zones"    
   - ouvrir le projet java "open-merx-online\Server\SFSExtensions"
   - ajouter sfs2x.jar et sfs2x-core.jar au build path
   - Modifier l'option "Compiler compliance level" et choisir 1.8  
   - Exporter le projet en .jar dans "<installSmartfox>\SFS2X\extensions\openMerxOnline\OpenMerxServerExtension.jar"
   - lancer smartfoxserver (\SmartFoxServer_2X\SFS2X\sfs2x.bat) qui gere les communications
 - Préparer l'environnement du serveur c# (Gestion de l'univers)
   - Ajouter une variable d'environement openmerx_home pointant vers la racine du dépot (ex : openmerx_home=D:\Sandbox\Unity3DFrance\open-merx-online)
   - ouvrir la solution "open-merx-online\Server\solutions\vs2017\MerxOnlineServer\MerxOnlineServer.sln"
   - compiler la solution (Release ou Debug en fonction du besoin)
   - lancer le projet ServerApp.exe pour gerer l'univers du jeu (open-merx-online\Server\solutions\vs2017\MerxOnlineServer\MerxGameServer\bin\Release or Debug)

## Choix d'une contribution

Le projet contient une section « Issues ». Les issues sont soit des fonctionnalités envisagées ou des bugs sur l'existant. Les responsables du dépôt vont ajouter un tag "ToDo" aux Issues qui sont prêtes à être mises en œuvre. Simplement en choisir une et se lancer.

Les utilisateurs peuvent également créer des Issues qui seront analysées puis triées. Pendant ce processus, les utilisateurs peuvent également donner des avis. Tout cela est également une forme de contribution.

## Préparer la contribution

Comme le projet ne peut être modifié directement, l'utilisateur qui désire effectuer une modification devra en amont créer un "fork" du projet (une copie). Pour ce faire, simplement cliquer sur le mot "fork" en haut à droite dans « overview » du projet. Gitlab demandera possiblement le namespace sur lequel créer ce fork, le plus simple étant de choisir votre compte gitlab.

Une fois le fork effectué, vous aurez un dépôt, sur votre propre compte et sur lequel vous pourrez faire ce que vous voulez. Pour travailler sur le dépôt, effectuer un clone.

Il est recommandé de créer une branche pour les développements, mais ceci peut très bien être ignoré.

Vous effectuez vos modifications, vous faites des commits et des push comme n’importe quel dépôt git. Il est même possible de travailler à plusieurs sur ce dépôt si par exemple vous voulez implémenter une option à plusieurs.

## Faire une proposition

Quand vous avez terminé une modification que vous voulez intégrer dans le projet, vous devez faire un **merge request**. Pour ce faire : 

- Se placer sur votre dépôt dans l'interface web gitlab

- Choisir la bonne branche au besoin

- Ouvrir le menu **Merge Requests** à gauche.

- Choisir **New merge request**

- Dans la page suivante, choisir comme branche source la vôtre

- La branche cible : Unity3D-FrancophoneMerx Online : master

- Appuyer **compare branchs and continue**

- Vérifier la liste des commits et changement qui ont été trouvés

- Entrer un titre et une description, indiquer le numéro de l'issue si possible

- Appuyer **Submit merge requets**

- Attendre les avis ou demandes de correctifs si besoin

# Ce qui peut ou pas être dans une contribution

Tout ce que vous apportez comme contribution doit respecter la loi. Vous devez avoir le droit de faire l’apport et si besoin le faire en respectant les règles.

Une petite liste non exhaustive d’exemple de ce qui peut ou pas être fait :

- Il ne faut en aucun cas ajouter des assets du store Unity

- Pour ajouter une œuvre qui est une modification d’une autre œuvre publiée sous  une licence demandant l’attribution, ajouter l’information dans le fichier d’attribution (ATTRIBUTIONS.md).
