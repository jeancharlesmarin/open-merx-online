using System;

using MerxStructures;

namespace MerxData {

    /// <summary> Represente une usine dans une City. Les usines ont pour mission de permettre la transformation de resources</summary>
    [Serializable]
    public class Factory : GameEntity {

        /// <summary> le frame du debut de la production </summary>
        long _productionStart;
        /// <summary> le nombre de run consecutive a effectuer de la recette lancee </summary>
        int _nbRun = 0;
        /// <summary> la recette actuellement en cours de production </summary>
        Recipe _currentRecipe = null;
        /// <summary> le hangar ou prendre et stocker les resources </summary>
        Hangar _procutionHangar = null;

        /// <summary> La City dans laquel se situe cette factory </summary>
        City _city = null;

        /// <summary> Indique si l'usine est presentement en cours de fonctionnement </summary>
        public bool Running { get { return _productionStart != 0;  } }

        /// <summary> Construire une factory </summary>
        /// <param name="City"> la City dans laquelle existe cette usine</param>
        public Factory(City city) : base(city.Universe) {
            _city = city;
        }

        /// <summary> permet a l'usine de se mettre a jour, appel� seulement si quelqu'un a enregistr� un evenement dans l'univers </summary>
        /// <param name="currentFrame">Le frame a laquelle la modification survient</param>
        /// <returns></returns>
        public override void Update(long currentFrame) {
            if(_productionStart != 0 && _currentRecipe != null && currentFrame >= _productionStart + _currentRecipe.Time) {
                //production finie

                Hangar h = _city.GetHangar(0);
                h.AddResources(_currentRecipe.outputs);

                if (_nbRun == 1) {
                    _productionStart = 0;
                    _procutionHangar = null;
                    _currentRecipe = null;
                    _nbRun = 0;
                } else {
                    Start(h, _currentRecipe, _nbRun - 1);
                }
            }
        }

        /// <summary> Lancer une recette pour un certain nombre de run </summary>
        public void Start(Hangar hangar, Recipe recipe, int nbRun) {
            _nbRun = 0;
            _productionStart = 0;
            _currentRecipe = null;            
            _procutionHangar = null;

            if (null == hangar || nbRun < 1 || null == recipe)
                return;

            //verifier si les resources sont disponible
            if (!hangar.TestQtes(recipe.inputs)) {
                return;
            }

            _currentRecipe = recipe;
            _nbRun = nbRun;
            _procutionHangar = hangar;
            //retirer les resources du hangar
            hangar.RemoveResources(recipe.inputs);
            
            //lancer la production
            _productionStart = Universe.Frame;

            //indiquer a l'universe de nous avertir quand le frame de fin est atteint
            Universe.RegisterEvent(_productionStart + _currentRecipe.Time, this);
        }
    }
}