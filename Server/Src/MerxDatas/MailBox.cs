using System;
using System.Collections.Generic;

using MerxStructures;

namespace MerxData {

    /// <summary>
    /// Represente la boite de courrier d'nue corporation
    /// </summary>
    [Serializable]
    public class MailBox {
        public List<MailInfos> _mails = new List<MailInfos>();

        private Corporation _corp = null;
        private int _mailID = 0;

        public MailBox(Corporation corp) {
            _corp = corp;
        }

        public void AddMail(MailInfos mail) {
            mail.Read = false;
            mail.Id = _mailID++;
            _mails.Add(mail);
            SendChanges();
        }

        public void ReadMail(int id) {
            foreach(MailInfos m in _mails) {
                if(m.Id == id){
                    m.Read = true;
                    SendChanges();
                }
            }
        }
        public void DeleteMail(MailInfos mail) {
            foreach(MailInfos m in _mails) {
                if(m.Id == mail.Id) {
                    _mails.Remove(m);
                    SendChanges();
                    break;
                }
            }
        }

        private void SendChanges() {
            MerxMessage.RequestMailBoxUpdate r = new MerxMessage.RequestMailBoxUpdate();

            foreach(MailInfos m in _mails) {
                r.infos.mails.Add(m);
            }

            r.SetTargetToPlayerID(_corp.CorpID);

            _corp.Universe.messageToSend.Enqueue(r);
        }

    }
}