using System;
using System.Collections.Generic;

namespace MerxData {

    /// <summary>
    /// Represente un hangar. Il peut contenir des vaisseaux ou des tas de ressources(Stack)
    /// </summary>
    [Serializable]
    public class Hangar : GameEntity{

        /// <summary> c'est temporairement le nombre de resource d'un type, todo utiliser des stacks</summary>
        Dictionary<int, int> _qtes = new Dictionary<int, int>();

        /// <summary> indique la quantite d'une ressource qui est dans le hangar mais qui n'est pas accessible (en gage pour une vente) </summary>
        Dictionary<int, int> _locks = new Dictionary<int, int>();

        /// <summary> liste des vaisseaux dans le hangars, les vaisseaux assembl� pret a fonctionner </summary>
        List<Vehicle> _vehicles = new List<Vehicle>();

        /// <summary> l'id a qui appartient ce hangar </summary>
        public int CorpID { get; private set; }

        /// <summary> la City dans laquel se situe ce hangar </summary>
        public City City { get; private set; }

        /// <summary>
        /// Creation d'un hangar
        /// </summary>
        /// <param name="universe">l'univers dana laquelle existe ce hangar</param>
        public Hangar(City city, int corpID) : base(city.Universe) {
            City = city;
            CorpID = corpID;
        }

        /// <summary> ici le hangar effectue les taches qu'il doit effectuer a un temps donn� comme la facturation par exemple</summary>
        /// <param name="currentFrame"></param>
        public override void Update(long currentFrame) {
        }

        /// <summary> permet de tester si un joueur peut utiliser un hangar en fournissant son playerID </summary>
        /// <param name="playerID"></param>
        /// <returns></returns>
        public bool CanUseHangar(int playerID) {
            return true; //todo : gerer les droits aux hangars par corp
        }

        /// <summary> Ajouter des resources dans ce hangar </summary>
        /// <param name="resourceType">Le type de la resource</param>
        /// <param name="qte">la quantite a ajouter</param>
        public void AddResource(int resourceType, int qte) {
            if (_qtes.ContainsKey(resourceType)){
                _qtes[resourceType] += qte;
            } else {
                _qtes.Add(resourceType, qte);
            }
            SendChange();
        }

        /// <summary>
        /// permet de retirer une quantite de ressources du hangars
        /// </summary>
        /// <param name="qtes">dictionaire, cle etant le type de resources et les valeurs les quantites</param>
        public void RemoveResources(Dictionary<int,int> qtes) {
            if (TestQtes(qtes)) {
                foreach(var q in qtes) {
                    if (_qtes[q.Key] == q.Value) {
                        _qtes.Remove(q.Key);
                    } else {
                        _qtes[q.Key] -= q.Value;
                    }
                }
                SendChange();
            }            
        }

        public void RemoveResources(int resType, int qte) {
            AddResource(resType, -qte);
        }

        /// <summary>
        /// permet d'ajouter un ensemble de resource a ce hangar
        /// </summary>
        /// <param name="qte">un dictionary des resources a ajouter, les cle etant les id et les valeurs les quantites</param>
        public void AddResources(Dictionary<int,int> qtes) {
            foreach(var p in qtes) {
                if (_qtes.ContainsKey(p.Key)) {
                    _qtes[p.Key] += p.Value;
                } else {
                    _qtes.Add(p.Key, p.Value);
                }
            }
            SendChange();
        }


        public Vehicle GetVehicle(int id) {
            foreach(Vehicle v in _vehicles) {
                if (v.ID == id)
                    return v;
            }
            return null;
        }

        public void AddVehicle(Vehicle v) {
            _vehicles.Add(v);
            SendChange();
        }

        public void RemoveVehicle(int vehicleID) {
            for (int i = _vehicles.Count-1; i >= 0; i--) {
                if (_vehicles[i].ID == vehicleID) {
                    _vehicles.RemoveAt(i);
                }
            }
            SendChange();
        }

        /// <summary> Recuperer la quantit� d'une certaine resource dans ce hangar </summary>
        /// <param name="resourceType">l'id du type de resource a tester</param>
        /// <returns></returns>
        public int GetResourceCount(int resourceType) {
            if (_qtes.ContainsKey(resourceType)) {
                return _qtes[resourceType];
            }
            return 0;
        }

        public Dictionary<int,int> GetResources() {
            return _qtes;
        }

        public int GetLockREsources(int resType) {
            if (_locks.ContainsKey(resType))
                return _locks[resType];
            return 0;
        }

        public void LockResources(int resType, int qte) {
            if(_qtes.ContainsKey(resType) && _qtes[resType] >= qte) {
                if (!_locks.ContainsKey(resType))
                    _locks.Add(resType, 0);
                _locks[resType] += qte;

                _qtes[resType] -= qte;
                if (_qtes[resType] <= 0)
                    _qtes.Remove(resType);
            }
        }

        /// <summary> deverouille des ressources et les ajoute dans la quantite du hangar </summary>
        /// <param name="resType"></param>
        /// <param name="qte"></param>
        public void UnlockResources(int resType, int qte) {
            if(_locks.ContainsKey(resType) && _locks[resType] >= qte) {
                _locks[resType] -= qte;
                if (_locks[resType] <= 0)
                    _locks.Remove(resType);
                if (!_qtes.ContainsKey(resType))
                    _qtes.Add(resType, qte);
                else {
                    _qtes[resType] += qte;
                }
            }
        }

        /// <summary> instancier un vaisseau dans ce hangar, c'est prendre une vaiseau en kit et le transformer env viasseau utilisable </summary>
        /// <returns></returns>
        public Vehicle AssembleVehicle(int vehiculeType) {

            //todo test qtes
            RemoveResources(vehiculeType, 1);

            int id = Universe.NextUID;
            Vehicle result = new Vehicle(Universe);
            _vehicles.Add(result);


            SendChange();

            return result;
        }

        public void RepackageVehicle(int vehicleID) {

            foreach(Vehicle v in _vehicles) {
                if(v.ID == vehicleID) {

                    // todo : gerer le type de vehicle
                    AddResource(3, 1);
                    
                    _vehicles.Remove(v);
                    // todo : detruire le vehicle                    

                    SendChange();

                    return;
                }
            }
        }

        /// <summary>
        /// permet de tester si un ensemble de resources sont disponible dans le hangar
        /// </summary>
        /// <param name="qtes">dictionaire ou cle est le type de resource et la valeur la quantite de ressource demandee</param>
        /// <returns>true si pour toutes les resources la quantite est suffisante</returns>
        public bool TestQtes(Dictionary<int,int> qtes) {

            foreach(var q in qtes) {
                if (!_qtes.ContainsKey(q.Key) || q.Value > _qtes[q.Key])
                    return false;
            }

            return true;
        }


        private void SendChange() {
            MerxMessage.RequestHangarUpdate update = new MerxMessage.RequestHangarUpdate();
            update.IsAnswer = true;
            update.infos = GetInfos();

            update.SetTargetToPlayerID(CorpID);
            Universe.messageToSend.Enqueue(update);
        }

        public MerxStructures.HangarInfos GetInfos() {

            MerxStructures.HangarInfos result = new MerxStructures.HangarInfos(ID, CorpID, City.ID);
            result.qtes = _qtes;
            foreach (Vehicle v in _vehicles) {
                result.vehicles.Add(v.ID);
            }

            return result;
        } 
        #region comparaisons
        public override int GetHashCode() {
            return ID;
        }

        public override bool Equals(object obj) {
            if (!base.Equals(obj))
                return false;

            if (null == obj)
                return false;

            Hangar other = obj as Hangar;
            if (null == other)
                return false;

            if (other._qtes.Count != _qtes.Count)
                return false;

            foreach(int k in other._qtes.Keys) {
                if (!_qtes.ContainsKey(k))
                    return false;

                if (other._qtes[k] != _qtes[k]) 
                    return false;

                if (other.CorpID != CorpID)
                    return false;
            }
            return true;
        }
        #endregion

    }
}