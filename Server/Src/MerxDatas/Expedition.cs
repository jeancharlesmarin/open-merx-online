using System;
using System.Collections.Generic;

namespace MerxData {

    /// <summary>
    /// represente une expedition en cours. elle est responsable de se mettre a jour a un certain nombre d'evenement pendant
    /// l'expedition et de determiner le resultat final au terme de l'expedition
    /// </summary>
    [Serializable]
    public abstract class Expedition : GameEntity{

        protected long _startFrame = 0;
        protected long _endFrame = 0;
        protected List<Vehicle> _vehicles = new List<Vehicle>();
        protected Hangar _startHangar = null;
        protected City _startCity = null;

        /// <summary> creation d'une expedition </summary>
        public Expedition(Universe universe) : base(universe) {
        }

        public override void Update(long currentFrame) {
            if (currentFrame >= _endFrame) {
                Finish();
            }
        }

        //lance l'expedition en retirant les vehicules des hangars
        public bool Start(City startCity, Hangar startHangar, List<Vehicle> vehicles) {

            _vehicles = new List<Vehicle>(vehicles);
            _startHangar = startHangar;
            _startCity = startCity;
            _startFrame = Universe.Frame;
            _endFrame = _startFrame + 1000;

            //tester si il y a assez de fuel dans le hangar, si oui, faire le plein et les supprimer


            // preparer l'event de la fin de ma mission
            Universe.RegisterEvent(_endFrame, this);

            // enlever du hangars chaque v�hicles impliqu�s dans la mission
            foreach (Vehicle v in _vehicles) {
                _startHangar.RemoveVehicle(v.ID);
            }

            return true;
        }

        /// <summary>
        /// le processus quand l'expedition est termine
        /// </summary>
        protected void Finish() {
            //remettre les vehicles dans les hangars
            foreach (Vehicle v in _vehicles) {
                _startHangar.AddVehicle(v);
            }

            //determiner le resultat
            GenerateReward();

            Universe.DeleteEntity(this);
        }

        abstract protected void GenerateReward();
    }
}