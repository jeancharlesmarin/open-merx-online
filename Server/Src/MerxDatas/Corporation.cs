using System;

namespace MerxData {

    /// <summary>
    /// Represente une corporation dans le jeu, la corporation est l'entity qui permet de mesurer la richesse d'un joueur
    /// dans l'etat actuel, une corporation == un joueur, si la corporation fait faillite, elle est reinitialise et ses actifs detruits.
    /// </summary>
    [Serializable]
    public class Corporation  : GameEntity{
        
        /// <summary> la quantite d'ICU dans le wallet de la corporation </summary>
        long _icu = 0;
        public long Icu {
            get { return _icu; }
            set { _icu = value; OnChange(); }
        }

        /// <summary> un ID unique reserve au corporation </summary>
        public int CorpID { get; private set; }

        /// <summary> la boite mail de la corporation </summary>
        private MailBox _mailBox = null;

        /// <summary> Construction </summary>
        /// <param name="corpID">un ID pour la corporation, permet au client de savoir s'il s'agit d'une corpo ou une autre</param>
        /// <param name="universe">L'univers dans lequel se trouve cette corporation </param>
        public Corporation(int corpID, Universe universe) : base(universe) {
            CorpID = corpID;
        }

        /// <summary>  Un corporation n'a pas de cas d'update, mais si besoin la methode est la </summary>
        /// <param name="currentFrame"></param>
        public override void Update(long currentFrame) {
        }

        public MailBox GetMailBox() {
            if (null == _mailBox)
                _mailBox = new MailBox(this);
            return _mailBox;
        }

        /// <summary> Quand la corporation change, c'est ici qu'on envoie les messages de mise a jour </summary>
        private void OnChange() {
            MerxMessage.RequestCorporationUpdate msg = new MerxMessage.RequestCorporationUpdate();
            msg.IsAnswer = true;

            msg.corpInfo = new MerxStructures.CorporationInfos();
            msg.corpInfo.CorpID = CorpID;
            msg.corpInfo.Icu = Icu;
            msg.corpInfo.Name = "";

            msg.SetTargetToPlayerID(CorpID);

            Universe.messageToSend.Enqueue(msg);
        }
    }
}