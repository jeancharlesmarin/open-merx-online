﻿using NUnit.Framework;

using MerxStructures;

namespace MerxData.tst {

    [TestFixture]
    public class FactoryTest {        

        private Universe _universe = null;
        private City _city0 = null;

        [SetUp]
        public void Init() {
            _universe = new Universe(0);

            _city0 = _universe.GetCity("Albanel");
        }

        [Test]
        public void SimpleProduction() {
            City s = _universe.GetCity("Dolbeau");
            Factory f = s.GetFactory(0);
            Hangar h = s.GetHangar(0);
            h.AddResource(1, 100);

            Recipe r = new Recipe();
            r.Time = 100;
            r.inputs.Add(1, 50);
            r.outputs.Add(2, 100);

            f.Start(h,r, 1);
            Assert.IsTrue(f.Running);
            Assert.AreEqual(50, h.GetResourceCount(1));

            _universe.PlayXFrames(100);

            Assert.IsFalse(f.Running);
            //            Assert.AreEqual(100, h.GetResourceCount(2));
            Assert.AreEqual(1, _universe.Market.GetSellOrder(2).Count);
        }

        [Test]
        public void ProductionNoResources() {

            Factory f = _city0.GetFactory(0);
            Hangar h = _city0.GetHangar(0);
            Recipe r = new Recipe();
            r.Time = 100;
            r.inputs = new System.Collections.Generic.Dictionary<int, int>();
            r.inputs.Add(1, 50);

            f.Start(h,r,1);
            Assert.IsFalse(f.Running);
        }

        [Test]
        public void ProductionAvecInputs() {

            Factory f = _city0.GetFactory(0);
            Hangar h = _city0.GetHangar(0);
            h.AddResource(2, 100);
            Recipe r = new Recipe();
            r.Time = 100;
            r.inputs.Add(2, 100);

            f.Start(h, r, 1);

            _universe.PlayXFrames(100);

            Assert.AreEqual(0, h.GetResourceCount(2));
        }

    }
}
