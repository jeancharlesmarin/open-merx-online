﻿using NUnit.Framework;

using MerxStructures;

namespace MerxData.tst {

    [TestFixture]
    public class CityTest {

        Universe _universe = null;

        [SetUp]
        public void Init() {
            _universe = new Universe(0);
        }


        [Test(Description = "city prod + market")]
        public void CityProduceAndSell() {
            //ici on test que la city a bien son hangar
            City albanel = _universe.GetCity("Albanel");
            Hangar cityHangar = albanel.GetHangar(0);
            cityHangar.AddResource(1, 100);

            Factory f = albanel.GetFactory(0);
            Recipe r = new Recipe();
            r.inputs = new System.Collections.Generic.Dictionary<int, int>();
            r.inputs.Add(1, 50);
            r.outputs = new System.Collections.Generic.Dictionary<int, int>();
            r.outputs.Add(2, 100);
            r.Time = 1000;

            f.Start(cityHangar, r, 1);


            Assert.AreEqual(50, cityHangar.GetResourceCount(1));

            for (int i = 0; i < 1001; i++) {
                _universe.PlayOnFrame();
            }

            Assert.AreEqual(0 , cityHangar.GetResourceCount(2));

            //ajoute au market
            Market m = _universe.Market;
            Assert.AreEqual(1, m.GetSellOrder(2).Count);

        }
    }
}
