using System.Collections.Generic;

using MerxMessage;

namespace MerxServer {

    /// <summary>
    /// interface que doit implementer les objets qui permettent de recevoir des requetes des clients
    /// </summary>
    public interface IRequestReceiver {

        /// <summary>
        /// permet de recuperer la liste de nouveau message recu depuis le dernier appel a cette m�thode
        /// </summary>
        /// <returns>les message qui ont �t� recus</returns>
        Queue<Message> GetMessages();
    }
}