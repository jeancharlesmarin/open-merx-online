using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

using MerxMessage;

namespace MerxServer {
    /// <summary>
    /// implementation d'un RequestReceiver qui permet de recevoir des event en local par un stream
    /// </summary>
    public class LocalRequestReceiver : IRequestReceiver {

        /// <summary> le stream a utiliser pour recevoir les donn�es brut </summary>
        private Queue<MemoryStream> _inStream = null;

        /// <summary> un thread qui va lire les donn�es sur le stream d'entree en boucle </summary>
        private Thread _readThread = null;
        
        /// <summary> La liste des message recus qui n'ont pas encore ete r�cup�r� </summary>
        private Queue<Message> _message = new Queue<Message>();

        /// <summary> Permet de connaitre l'id du player local (cela est utilis� lors des comunications </summary>
        public int LocalPlayerID { get; set; }

        /// <summary>
        /// constructeur d'un receiver pour un stream donn�
        /// </summary>
        /// <param name="stream">le memorystream sur lequel lire</param>
        public LocalRequestReceiver(Queue<MemoryStream> streams) {
            _inStream = streams;

            _readThread = new Thread(new ThreadStart(ReadingLoop));
            _readThread.Start();
        }


        /// <summary>
        /// la boucle qui sera ex�cut� par le thread de lecture
        /// </summary>
        private void ReadingLoop() {
            while (true) {
                Thread.Sleep(0);
                lock (_inStream) {
                    if (_inStream.Count > 0) {
                        MemoryStream ms = _inStream.Dequeue();
                        Message m = Message.Deserialize(ms.ToArray());
                        m.FromPlayerID = LocalPlayerID;
                        lock (_message) {
                            _message.Enqueue(m);
                        }                        
                    }
                }
            }
        }

        /// <summary>
        /// r�cuperer les messages et vider la queue de message
        /// </summary>
        /// <returns>une queue des messages qui n'ont pas ete traite</returns>
        public Queue<Message> GetMessages() {
            lock (_message) {
                Queue<Message> result = _message;
                _message = new Queue<Message>();

                return result;
            }
        }
    }
}