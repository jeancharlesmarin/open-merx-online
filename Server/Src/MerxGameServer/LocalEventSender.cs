using System.Collections.Generic;
using System.IO;

using MerxMessage;

namespace MerxServer {
    /// <summary>
    /// Classe permetant d'envoyer des evenements au client et ce en local dans le meme processus.
    /// La communication fonctionne � l'aide de memoryStream
    /// </summary>
    public class LocalEventSender : IEventSender{
        
        /// <summary> Le stream a utilier pour communiquer </summary>
        private Queue<MemoryStream> _outStream = null;

        /// <summary>
        /// construire un EventSender a partir d'un memoryStream
        /// </summary>
        /// <param name="stream">le stream a utiliser les communication de cet objet</param>
        public LocalEventSender(Queue<MemoryStream> stream) {
            _outStream = stream;
        }


        /// <summary>
        /// implementation de la methode d'envoie de message aux clients
        /// </summary>
        /// <param name="message">le message a envoyer</param>
        public void SendMessage(Message message) {

            byte[] data = message.Serialize();
            MemoryStream stream = new MemoryStream(data);
            lock (_outStream) {
                _outStream.Enqueue(stream);
            }
        }
    }
}