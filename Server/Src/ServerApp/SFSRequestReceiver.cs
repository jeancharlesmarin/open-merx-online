﻿using System.Collections.Generic;

using MerxServer;
using MerxMessage;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    class SFSRequestReceiver : IRequestReceiver {

        SFSConnection _conn = null;
        Queue<Message> _messages = new Queue<Message>();

        public SFSRequestReceiver(SFSConnection conn) {
            _conn = conn;
            _conn.OnNewMessage += _conn_OnNewMessage;
        }

        private void _conn_OnNewMessage(Message m) {
            lock (_messages) {
                _messages.Enqueue(m);
            }
        }

        public Queue<Message> GetMessages() {
            Queue<Message> result = null;

            lock (_messages) {
                 result = _messages;

                _messages = new Queue<Message>();
            }

            return result;
        }
    }
}
