﻿using System;

using MerxServer;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Logging;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Security.Cryptography;
using System.Text;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    class SFSConnection {

        private SmartFox _smartFox = null;
        private SFSEventSender _sender = null;
        private SFSRequestReceiver _receiver = null;
        private bool _ready = false;
        private DateTime _lastSend = DateTime.Now;

        public delegate void OnNewMessageAction (MerxMessage.Message m);
        public event OnNewMessageAction OnNewMessage = delegate { };

        public SFSConnection() {
            //lancer la connection
            _smartFox = new SmartFox();

            Sfs2X.Util.ConfigData cfg = new Sfs2X.Util.ConfigData();
            //cfg.Host = "127.0.0.1";
            cfg.Host = "51.254.200.78";
            cfg.Port = 9933;
            cfg.Zone = "OpenMerx";
            cfg.Debug = false;

            _lastSend = DateTime.Now;

            _smartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            _smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
            _smartFox.AddEventListener(SFSEvent.LOGIN, OnLogin);
            _smartFox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            _smartFox.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
            _smartFox.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
            _smartFox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtension);

            _smartFox.AddLogListener(LogLevel.INFO, OnInfoMessage);
            _smartFox.AddLogListener(LogLevel.WARN, OnWarnMessage);
            _smartFox.AddLogListener(LogLevel.ERROR, OnErrorMessage);

            Console.WriteLine("Starting connection");

            _smartFox.Connect(cfg);

        }

        public void Process() {
            if (_smartFox != null) { 
                _smartFox.ProcessEvents();
            }
            if( (DateTime.Now - _lastSend).Seconds > 30) {
                MerxMessage.Message m = new MerxMessage.RequestUniverseTime();
                m.SetTargetToPlayerID(-1);
                _sender.SendMessage(m);
                _lastSend = DateTime.Now;
            }
        }

        public IEventSender GetSender() {
            return _sender;
        }

        public IRequestReceiver GetReceiver() {
            return _receiver;
        }

        public bool Ready() {
            return _ready;
        }

        private void Reset() {

        }

        void OnConnection(BaseEvent evt) {
            Console.WriteLine("OnConnection");
            if ((bool)evt.Params["success"]) {
                Console.WriteLine("Connection established successfully");
                Console.WriteLine("SFS2X API version: " + _smartFox.Version);
                Console.WriteLine("Connection mode is: " + _smartFox.ConnectionMode);

                string cryptedPassword = MerxUtils.MerxCrypto.ComputeSha256Hash("test");
                _smartFox.Send(new Sfs2X.Requests.LoginRequest("Server", cryptedPassword));
            } else {
                Console.WriteLine("Connection failed; is the server running at all?");

                // Remove SFS2X listeners and re-enable interface
                Reset();
            }
        }

        void OnConnectionLost(BaseEvent evt) {
            Console.WriteLine("OnConnectionLost");
        }

        void OnLogin(BaseEvent evt) {
            Console.WriteLine("OnLogin");
            User user = (User)evt.Params["user"];

            // Show system message
            string msg = "Connection established successfully\n";
            msg += "SFS2X API version: " + _smartFox.Version + "\n";
            msg += "Connection mode is: " + _smartFox.ConnectionMode + "\n";
            msg += "Logged in as " + user.Name;
            Console.WriteLine(msg);


            _smartFox.Send(new Sfs2X.Requests.JoinRoomRequest("The Lobby"));
        }

        void OnLoginError(BaseEvent evt) {
            Console.WriteLine("OnLoginError");
        }

        void OnRoomJoin(BaseEvent evt) {
            _sender = new SFSEventSender(_smartFox);
            _receiver = new SFSRequestReceiver(this);
            _ready = true;

            Console.WriteLine("OnRoomJoin");
        }

        void OnRoomJoinError(BaseEvent evt) {
            Console.WriteLine("OnRoomJoinError");
        }

        void OnInfoMessage(BaseEvent evt) {
            Console.WriteLine("OnInfoMessage");
        }

        void OnWarnMessage(BaseEvent evt) {
            Console.WriteLine("OnWarnMessage");
        }
        
        void OnErrorMessage(BaseEvent evt) {
            Console.WriteLine("OnErrorMessage");
        }

        void OnExtension(BaseEvent evt) {
            string cmd = (string)evt.Params["cmd"];
            SFSObject dataObject = (SFSObject)evt.Params["params"];

            if (cmd == "sendToServer") {
                Sfs2X.Util.ByteArray bytes = dataObject.GetByteArray("message");

                MerxMessage.Message m = MerxMessage.Message.Deserialize(bytes.Bytes);
                int senderID = dataObject.GetInt("playerID");

                if(null != m) {
                    m.FromPlayerID = senderID;
                    OnNewMessage(m);
                    Console.WriteLine("reveived : " + m.ToString());
                }                
            }
        }
    }
}
