﻿using System;
using System.Collections.Generic;

using MerxServer;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    class Program {
        static void Main(string[] args) {

            string filename = "save.dat";

            SFSConnection con = new SFSConnection();
            while (!con.Ready()) {
                con.Process();
                System.Threading.Thread.Sleep(0);
            }

            MerxServerApp server = null;
            if (System.IO.File.Exists(filename)) {
                server = new MerxServerApp(System.IO.File.ReadAllBytes(filename));
            } else {
                server = new MerxServerApp();
            }
            server.RequestReceiver = con.GetReceiver();
            server.EventSender = con.GetSender();
            server.Start();

            Console.WriteLine("Starting server");

            while (true) {
                con.Process();
                System.Threading.Thread.Sleep(0);
                if (System.IO.File.Exists("shutdown.txt"))
                    break;
            }

            Console.WriteLine("Shutting down and saving");
            
            server.Stop();
            if (System.IO.File.Exists(filename)) {
                string date = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"); 
                Console.WriteLine(date + "_" + filename);
                System.IO.File.Move(filename, date + "_" + filename);
            }

            System.IO.File.WriteAllBytes(filename, server.GetSaveData());

            System.IO.File.Delete("shutdown.txt");
        }
    }
}
