using System;
using System.Collections.Generic;

namespace MerxStructures {

    /// <summary>
    /// informations represenant une city, elle est utilis�e lors des communications entre
    /// clients et serveurs
    /// </summary>
    [Serializable]
    public struct CityInfo {
        /// <summary>
        /// l'identifiant unique de cette city
        /// </summary>
        public int ID;

        /// <summary>
        /// son nom
        /// todo : enlever l'utilisation du nom cote serveur
        /// </summary>
        public string Name;

        /// <summary>  la liste des ID de corporation ayant un hangar dans cette ville </summary>
        public List<int> Hangars;
    }
}