using System;

namespace MerxStructures {

    /// <summary>
    /// represente un offre d'achat ou de vente sur le marche.
    /// 
    /// </summary>
    [Serializable]
    public class MarketOrder {

        /// <summary> un id unique permetant de differencier les ordres du march�s </summary>
        public int id;

        /// <summary> est ce que c'est une offre d'achat (true) ou une offre de vente(false) </summary>
        public bool buying;

        /// <summary> le type de resource de cette offre </summary>
        public int resID;

        /// <summary> l'id de la city dans laquel cette offre est publiee </summary>
        public int cityID;

        /// <summary> la corporation qui a publie cette offre </summary>
        public int corpID;

        /// <summary> la quantite totale restante a acheter ou vendre en fonction de la situation </summary>
        public int qte;

        /// <summary> le prix demand� ou offert pour chaque unit� de resource </summary>
        public int price;

        /// <summary> le frame a lequel cet ordre a ete passe </summary>
        public long StartFrame;

        public MarketOrder(int iid, bool ibuying, int iresID, int icityID, int icorpID, int iqte, int iprice, long startFrame) {
            id = iid;
            buying = ibuying;
            resID = iresID;
            cityID = icityID;
            corpID = icorpID;
            qte = iqte;
            price = iprice;
            StartFrame = startFrame;
        }
    }
}