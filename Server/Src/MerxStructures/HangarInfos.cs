using System;
using System.Collections.Generic;

namespace MerxStructures {

    /// <summary>
    /// structure representant les informations d'un hangar, elle est utilis� lors de
    /// communications entre clients et serveurs
    /// </summary>
    [Serializable]
    public class HangarInfos {
        public int ID = 0;
        public int corpID = 0;
        public int cityID = 0;
        public Dictionary<int, int> qtes = new Dictionary<int, int>();
        public HashSet<int> vehicles = new HashSet<int>();

        public HangarInfos(int id, int corpID, int cityID) {
            ID = id;
            this.corpID = corpID;
            this.cityID = cityID;
        }
    }
}