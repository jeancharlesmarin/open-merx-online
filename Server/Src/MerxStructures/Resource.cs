using System;
using System.Collections.Generic;

namespace MerxStructures {

    /// <summary>
    /// L'information propre a une ressource. 
    /// </summary>
    [Serializable]
    public class Resource {

        /// <summary> l'id Unique de la ressource</summary>
        public int Id = 0;

        /// <summary>Le prix de base de l'item (si jamais plus aucun bien n'est sur le march� les IA vont utiliser ce prix</summary>
        public int BasePrice = 0;

        /// <summary> Le volume que prend 1 une unite de cette ressource </summary>
        public float Volume = 0.0f;
    }
}