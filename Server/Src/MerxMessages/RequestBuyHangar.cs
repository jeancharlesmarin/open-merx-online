using System;

using MerxStructures;

namespace MerxMessage {

    /// <summary>
    /// Un requete permetant a une corporation d'installer un hangar dans une city
    /// </summary>
    [Serializable]
    public class RequestBuyHangar : Message {

        /// <summary> La city ou acheter la hangar</summary>
        public int CityID = 0;

        /// <summary> l'id du hangar ainsi achete ou -1 si pas achete </summary>
        public int HangarID = -1;

        /// <summary> les infos du hangar cree </summary>
        public HangarInfos Hangar = null;
    }
}
        