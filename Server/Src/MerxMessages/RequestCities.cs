using System;
using System.Collections.Generic;

using MerxStructures;

namespace MerxMessage {

    /// <summary> un message pour recevoir la liste de toutes les city de l'univers </summary>
    [Serializable]
    public class RequestCities : Message {

        /// <summary> les cites  </summary>
        public List<CityInfo> cities = new List<CityInfo>();

        /// <summary>vla liste des sources des saut, l'indice 1 de gatesFrom a comme cible l'indice 1 de gatesTo (et vice verca) </summary>
        public List<int> gatesFrom = new List<int>();

        /// <summary> la liste des cibles des saut, l'indice 1 de gatesFrom a comme cible l'indice 1 de gatesTo (et vice verca) </summary>
        public List<int> gatesTo = new List<int>();

    }
}
        