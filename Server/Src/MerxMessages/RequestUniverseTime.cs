using System;

namespace MerxMessage {

    /// <summary>
    /// recuperer le frame actuel de l'univers, cela permet de mettre a jour le client le plus precisement possible
    /// </summary>
    [Serializable]
    public class RequestUniverseTime : Message {
        public long Frame { get; set; }
    }
}
        