using System;

using MerxStructures;

namespace MerxMessage {

    /// <summary>
    /// requete permetant de mettre a jour un hangar
    /// elle est envoy�e par un serveur aux joueur pour indiquer que l'etat d'un hangar a change
    /// todo il faut s'assurer qu'elle ne vient jamais d'un client mais bien d'un serveur
    /// </summary>
    [Serializable]
    public class RequestHangarUpdate : Message {

        public HangarInfos infos = null;
    }
}
        