using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// Requete permetant de demander l'assemblage d'un kit
    /// </summary>
    [Serializable]
    public class RequestRepackageVehicle : Message {

        /// <summary> la cite ou assembler un kit </summary>
        public int cityID = 0;

        /// <summary> l'id du vehicule dans le hangar a repackager </summary>
        public int vehicleID = 0;
    }
}
        