import java.util.Iterator;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class SendToClientsRequest extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject args) {

		Room room = this.getParentExtension().getParentRoom();
		int targetType = args.getInt("TargetType");
		String msgName = args.getUtfString("MsgName");
		
		int targetID = -1;
		if(args.containsKey("TargetID")) {
			targetID = args.getInt("TargetID");
		}
		
		switch(targetType) {
		case 1:
			System.out.println("SentToClient " + msgName + " from " + sender.getName() + " to all in room " + room.getName());
			break;
		case 2:
			System.out.println("SentToClient " + msgName + " from " + sender.getName() + " to player " + targetID + " in room "  + room.getName());
			break;			
		}
		
		
		

		// pour tout les user dans la room qui ne sont pas "Server" on envoie le message
		
		Iterator<User> ite = room.getUserList().iterator();
		while(ite.hasNext()) {
			User u = ite.next();
			String username = u.getName();
			
			int userID = -1;
			try {
				userID = DataManager.GetPlayerID(u.getName(),getParentExtension().getParentZone().getDBManager());
			} catch (Exception e) {
				System.out.println(e);
			}		    
		    
			if(!username.equals("Server")) {
				if(targetType == 1 || 							// to all
				   (targetType == 2 && userID == targetID)) {	// to playerID
					
				
					System.out.println("Sendind to " + username);
					send("sendToClients", args, u);
				}
			}
		}
	}

}
